package it.dtk;

import java.util.Properties;

import cascading.flow.Flow;
import cascading.flow.FlowDef;
import cascading.flow.hadoop.HadoopFlowConnector;
import cascading.operation.aggregator.Count;
import cascading.operation.regex.RegexSplitGenerator;
import cascading.pipe.Each;
import cascading.pipe.Every;
import cascading.pipe.GroupBy;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.Scheme;
import cascading.scheme.local.TextLine;
import cascading.tap.Tap;
import cascading.tap.hadoop.Hfs;
import cascading.tuple.Fields;

public class WordCount {

	public static void main(String[] args) {
		String inputPath = args[0];
		String outputPath = args[1];
		
		Scheme sourceScheme = new TextLine(new Fields("line"));
		Tap source = new Hfs(sourceScheme, inputPath);
		
		Scheme outputScheme = new TextLine(new Fields("word","count"));
		Tap output = new Hfs(outputScheme, outputPath);
		
		Fields line = new Fields("line");
		Fields word = new Fields("word");
		
		RegexSplitGenerator splitter = new RegexSplitGenerator(word,"[ \\[\\]\\(\\),.]");
		
		//leggi ogni linea e la splitta in base allo splitter e lo restituisce come output
		Pipe loadPipe = new Each("load", line, splitter, Fields.RESULTS);
		
		//esegue il word count
		Pipe wcPipe = new Pipe("wordcount", loadPipe);
		wcPipe = new GroupBy(wcPipe,word);
		wcPipe = new Every(wcPipe, word,new Count(), new Fields("count"));
		
		Properties properties = new Properties();
	    AppProps.setApplicationJarClass( properties, WordCount.class );
	    HadoopFlowConnector flowConnector = new HadoopFlowConnector( properties );
		
	    FlowDef flowDef = FlowDef.flowDef()
	    		.setName("wordcount")
	    		.addSource(loadPipe, source)
	    		.addSink(wcPipe, output);
	    
	    Flow wcFlow = flowConnector.connect(flowDef);
	    wcFlow.writeDOT("dot/wc.dot");
	    wcFlow.complete();
	}
}
