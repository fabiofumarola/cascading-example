package it.dtk;

import java.util.Properties;

import cascading.flow.hadoop.HadoopFlowConnector;
import cascading.operation.Aggregator;
import cascading.operation.aggregator.Count;
import cascading.operation.regex.RegexSplitGenerator;
import cascading.pipe.*;
import cascading.property.AppProps;
import cascading.scheme.Scheme;
import cascading.scheme.local.TextLine;
import cascading.tap.SinkMode;
import cascading.tap.Tap;
import cascading.tap.hadoop.Hfs;
import cascading.tuple.Fields;

public class WordCount3 {

	public static void main(String[] args) {
		String inputPath = args[0];
		String outputPath = args[1];

		//definisce lo schema dei dati da leggere individuando una sola colonna denominata linea
		Scheme sourceScheme = new TextLine(new Fields("line"));
		Tap source = new Hfs(sourceScheme, inputPath);
		
		//definisce l'output sotto forma di due colonne word e count resi sottoforma di fields
		//con  SinkMode.REPLACE l'output folder viene cancellato e sostituito
		Scheme sinkScheme = new TextLine( new Fields("word", "count"));
		Tap sink = new Hfs(sinkScheme, outputPath, SinkMode.REPLACE);
		
		//definisce l'inizio della pipeline
		Pipe pipe = new Pipe("wordcount");
		
		//definisce lo splitter che in base alla expressione regolare crea le word per il sink
		RegexSplitGenerator splitter = new RegexSplitGenerator(new Fields("word"), "[ \\[\\]\\(\\),.]");
		//per ogni linea nel file di input applica lo splitter
		pipe = new Each(pipe, new Fields("line"), splitter);
		
		//raggruppa le parole in base alla word
		pipe = new GroupBy(pipe, new Fields("word"));
		
		//aggrega in base le word e genera il field count
		Aggregator count = new Count(new Fields("count"));
		pipe = new Every(pipe, count);
		
		Properties properties = new Properties();
		AppProps.setApplicationJarClass(properties, WordCount2.class);
		// create
		HadoopFlowConnector flowConnector = new HadoopFlowConnector(properties);
		
		
	}
}
