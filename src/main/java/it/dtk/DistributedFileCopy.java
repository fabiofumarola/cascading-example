package it.dtk;

import java.util.Properties;

import cascading.flow.FlowDef;
import cascading.flow.hadoop.HadoopFlowConnector;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.hadoop.TextDelimited;
import cascading.tap.Tap;
import cascading.tap.hadoop.Hfs;

/**
 * @author fabio
 * example got to https://github.com/Cascading/Impatient/tree/master/part1
 */
public class DistributedFileCopy {

	public static void main(String[] args) {
		String input = args[0];
		String output = args[1];

		Properties properties = new Properties();
		AppProps.setApplicationJarClass(properties, DistributedFileCopy.class);
		// create
		HadoopFlowConnector flowConnector = new HadoopFlowConnector(properties);

		// create the source tap
		Tap inTap = new Hfs(new TextDelimited(true, "\t"), input);

		// create the sink tap
		Tap outTap = new Hfs(new TextDelimited(true, "\t"), output);

		// specify a pipe to connect the taps
		Pipe copyPipe = new Pipe("copy");

		// connect the taps, pipes into a flow
		FlowDef flowDef = FlowDef.flowDef().addSource(copyPipe, inTap)
				.addTailSink(copyPipe, outTap);
		
		flowConnector.connect(flowDef).complete();
	}
}
