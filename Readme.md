Cascading
========
Cascading is a data processing API and processing query planeer used for defining, sharing, and executing data-processing workflows.
On top of Hadoop Cascading adds an abstraction layer over the Hadoop API, simplifying Hadoop application development, job creation,
and job scheduing.

Data Processing Workflow
--------

the workflow is composed by Operation, Tuple and Field. The operation are represented as 
pipelines which elaborate Tuples composed by fields.

Pipes
-------

The base class for the pipes is cascading.pipe.Pipe and it has as sublcasses: Each, Merge, GroupBy, Join, CoGroup, Every SubAssembly.

1. The Each pipe allows to apply functions or filter to each tuple in the streams. The Each operator applies either a Function or a Filter to each entry in the Tuple stream. 
	Any number of Each operators can follow an Each, Splice, or Every operator. The Each operator can return more tuple for each input tuple.

2. The Merge Pipe allows for multiple branches, with the same fields to be spliced back into a single stream. 
	The behavior is similar to the GroupBy merging features, but Merge does not perform any grouping or sorting on keys. Thus, when using a MapReduce platform, no Reducer is required.
	Merge is non-blocking and performs no processing. Any number of branches and merges may be performed in a Flow without triggering any additional MapReduce jobs on the Hadoop platform.
	Unlike HashJoin, no preference need be made for left-hand or right-hand sided-ness of streams in relation to their sizes.

3. The GroupBy pipe groups the Tuple stream by the given groupFields. If more than one Pipe instance is provided on the constructor, 
	all branches will be merged. It is required that all Pipe instances output the same field names, otherwise the FlowConnector 
	will fail to create a Flow instance. Again, the Pipe instances are merged together as if one Tuple stream and not joined. 
	See CoGroup for joining by common fields. Typically an Every follows GroupBy to apply an Aggregator function to every grouping. 
	The Each operator may also follow GroupBy to apply a Function or Filter to the resulting stream. But an Each cannot come 
	immediately before an Every.Optionally a stream can be further sorted by providing sortFields. This allows an Aggregator 
	to receive values in the order of the sortedFields.
	
4. The CoGroup pipe is similar to GroupBy, but instead of a merge, performs a join. That is, CoGroup accepts two
    or more input streams and groups them on one or more specified keys, and performs a join operation on equal key
	values, similar to a SQL join.
 
 
 Difference between Every and Each
 
The key difference between Each and Every is that the Each operates on individual tuples, and Every operates
on groups of tuples emitted by GroupBy or CoGroup.
 
 Fields
 -------
 
 For more control over sorting at the group or secondary sort level, 
 use Fields containing Comparator instances for the appropriate 
 fields when setting the groupFields or sortFields values. 
 Fields allows you to set a custom Comparator instance for each field name or position. 
 It is required that each Comparator class also be Serializable.

http://docs.cascading.org/cascading/2.2/userguide/pdf/userguide.pdf
http://docs.cascading.org/cascading/2.2/javadoc/
